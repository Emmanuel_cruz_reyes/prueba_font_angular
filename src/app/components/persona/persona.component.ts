import { Component, Input, OnInit } from '@angular/core';
import { Persona } from 'src/app/classes/persona';
import { PersonaService } from 'src/app/service/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  personas: Persona[];
  defaultperson: Persona = {
    id: 1,
    nombre: 'Nombre',
    primer_apellido: 'apellido',
    segundo_apellido: 'apellido',
    telefono: 'numero',
    status: 'estado'
  };
  constructor(private personaService: PersonaService) { }

  ngOnInit(): void {

    this.personaService.getPersons().subscribe((data: Persona[]) => {
      console.log(data);
      this.personas = data;
    });

  }

  cambiarData(index: number) {
    this.defaultperson = this.personas[index];
  }

}
