export class Persona {
    id: number;
    nombre: string;
    primer_apellido: string;
    segundo_apellido: string;
    telefono: string;
    status: string;
}
