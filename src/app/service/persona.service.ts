import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Persona } from '../classes/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor( private http: HttpClient ) { }

  getPersons(): Observable<Persona[]>{
    return this.http.get<Persona[]>("//localhost:8080/api/v1/personas");
  }
}
